
const badgeElements = document.querySelectorAll('div.badge');
const badgesArray = Array.from(badgeElements);
const badges = {pending: 'badge-warning', paid: 'badge-success', submitted: 'badge-primary'};
badgesArray.map((element) => {
  const randomBadgeNumber = random(Object.keys(badges));
  const randomBadge = Object.keys(badges)[randomBadgeNumber];
  element.classList.add(badges[randomBadge]);
  element.innerText = randomBadge;
});


const authorElements = document.querySelectorAll('div.author');
const authorsArray = Array.from(authorElements);
const fakeNames = ['Mario Speedwagon', 'Petey Cruiser', 'Anna Sthesia', 'Paul Molive', 'Anna Mull', 'Gail Forcewind', 'Paige Turner', 'Bob Frapples'];
authorsArray.map((element) => {
  const randomNumber = random(fakeNames);
  const randomAuthor = fakeNames[randomNumber];
  element.innerHTML = `<strong>${randomAuthor}</strong>`;
});


const dateElements = document.querySelectorAll('time.date');
const dateArray = Array.from(dateElements);
const fakeDate = ['1 Aug, 12:00 — 14:30 GMT', '2 Aug, 11:00 — 12:30 GMT', 'Today, 12:00 — 14:30 GMT', '17 Aug, 12:00 — 14:30 GMT', '10 Aug, 17:00 — 18:30 GMT', '1 Aug, 11:00 — 12:30 GMT'];
dateArray.map((element) => {randomDataFromArray(element, fakeDate)});


const placeElements = document.querySelectorAll('div.place');
const placesArray = Array.from(placeElements);
const fakePlaces = ['Hogwarts School of Witchcraft and Wizardry', 'Willy Wonka\'s Factory', 'The Emerald City', 'The Galaxy', 'Gotham City', 'Xavier\'s Academy', 'Asgard', 'Neverland'];
placesArray.map((element) => {randomDataFromArray(element, fakePlaces)});



function random(arr) {
  return Math.floor(Math.random() * Math.floor(arr.length));
}

function randomDataFromArray (element, arr) {
  const randomNumber = random(arr);
  const randomItem = arr[randomNumber];
  element.innerText = `${randomItem}`
}